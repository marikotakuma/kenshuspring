package jp.co.kenshu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.form.TestForm;
import jp.co.kenshu.service.TestService;

@Controller
public class TestController {

	@Autowired
	private TestService testService;

	//全件表示のservlet
	@RequestMapping(value = "/test/", method = RequestMethod.GET)
	public String testAll(Model model) {
	    List<TestDto> tests = testService.getTestAll();
	    model.addAttribute("message", "MyBatisの全件取得サンプルです");
	    model.addAttribute("tests", tests);
	    return "testAll";
	}

	//登録のservlet
	@RequestMapping(value = "/test/insert/input/", method = RequestMethod.GET)
	public String testInsert(Model model) {
	    TestForm form = new TestForm();
	    model.addAttribute("testForm", form);
	    model.addAttribute("message", "MyBatisのinsertサンプルです。");
	    return "testInsert";
	}

	@RequestMapping(value = "/test/insert/input/", method = RequestMethod.POST)
	public String testInsert(@ModelAttribute TestForm form, Model model) {
	    int count = testService.insertTest(form.getName());
	    System.out.println("挿入件数は" + count + "件です。");
	    return "redirect:/test/";
	}

	//削除のservlet
	@RequestMapping(value = "/test/delete/input/", method = RequestMethod.GET)
	public String testDelete(Model model) {
	    TestForm form = new TestForm();
	    model.addAttribute("testForm", form);
	    model.addAttribute("message", "MyBatisのdeleteサンプルです。");
	    return "testDelete";
	}

	@RequestMapping(value = "/test/delete/input/", method = RequestMethod.POST)
	public String testDelete(@ModelAttribute TestForm form, Model model) {
	    int count = testService.deleteTest(form.getId());
	   System.out.println( "削除件数は" + count + "件です。");
	    return "redirect:/test/";
	}
}